const SITE_NAME = 'Carlisle Weather | ';

const routes = [
    {
        path: '/',
        name: 'Day',
        component: () => import('../components/day.js'),
        element: 'weather-day',
    },
    {
        path: '/day',
        name: 'Day',
        component: () => import('../components/day.js'),
        element: 'weather-day',
    },
    {
        path: '/week',
        name: 'Week',
        component: () => import('../components/week.js'),
        element: 'weather-week',
    },
    {
        path: '/month',
        name: 'Month',
        component: () => import('../components/month.js'),
        element: 'weather-month',
    },
    {
        path: '/year',
        name: 'Year',
        component: () => import('../components/year.js'),
        element: 'weather-year',
    },
];

export async function router(): Promise<string> {
    const newRoute = routes.find((route) => route.path === location.pathname);

    if (newRoute === null || newRoute === undefined) {
        throw new Error('Route not found');
    }

    await newRoute.component();

    document.title = SITE_NAME + newRoute.name;

    return newRoute.element;
}

export function navigateTo(url: string): void {
    history.pushState(undefined, '', url);
}
