import('./charts.js');

const template = document.createElement('template');
template.innerHTML = `
    <div>
        <nav style="text-align: center">
            <button id="backButton">
                <b>&lt;</b>
            </button>
            <span>Year: <span id="yearText"></span> Month: <span id="monthText"></span></span>
            <button id="forwardButton">
                <b>&gt;</b>
            </button>
        </nav>
        <weather-charts id="charts" weather-labels="[]" weather-data="[]" />
    </div>
    <link rel="stylesheet" href="https://cdn.simplecss.org/simple.min.css">
`;

export default class Month extends HTMLElement {
    weather: Record<string, any>[] = [];
    charts: HTMLElement;
    backButton: HTMLButtonElement;
    forwardButton: HTMLButtonElement;
    date: Date;
    labels = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12];
    monthText: HTMLElement;
    yearText: HTMLElement;
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.shadowRoot!.append(template.content.cloneNode(true));
        this.charts = this.shadowRoot?.getElementById('charts') as HTMLElement;
        this.backButton = this.shadowRoot?.getElementById(
            'backButton'
        ) as HTMLButtonElement;
        this.forwardButton = this.shadowRoot?.getElementById(
            'forwardButton'
        ) as HTMLButtonElement;
        this.monthText = this.shadowRoot?.getElementById(
            'monthText'
        ) as HTMLElement;
        this.yearText = this.shadowRoot?.getElementById(
            'yearText'
        ) as HTMLElement;

        this.date = new Date();
        this.backButton.addEventListener('click', () => {
            this.date.setMonth(this.date.getMonth() - 1);
            this.render();
        });
        this.forwardButton.addEventListener('click', () => {
            this.date.setMonth(this.date.getMonth() + 1);
            this.render();
        });

        this.render();
    }

    async fetchData(): Promise<void> {
        const response = await fetch(
            `https://weather.srchetwynd.co.uk/api/months?date=${
                this.date.toISOString().split('T')[0]
            }`
        );
        this.weather = await response.json();
        this.weather.sort((a, b) => {
            const aDate = new Date(a.date).getTime();
            const bDate = new Date(b.date).getTime();
            if (aDate > bDate) {
                return 1;
            } else if (aDate === bDate) {
                return 0;
            } else {
                return -1;
            }
        });
        for (const data of this.weather) {
            data.label = data.date.split(' ')[0];
        }
    }

    async render(): Promise<void> {
        if (this.date.getTime() >= Date.now()) {
            this.forwardButton.setAttribute('disabled', 'true');
        } else {
            this.forwardButton.removeAttribute('disabled');
        }

        await this.fetchData();
        this.monthText.innerHTML = String(this.date.getMonth() + 1);
        this.yearText.innerHTML = String(this.date.getFullYear());

        this.charts.setAttribute(
            'weather-labels',
            JSON.stringify(this.weather.map((data) => data.label))
        );
        this.charts.setAttribute('weather-data', JSON.stringify(this.weather));
    }
}

window.customElements.define('weather-month', Month);
