import { navigateTo } from '../router/index.js';
import RouterView from './router-view.js';

const template = document.createElement('template');
template.innerHTML = `
    <a><button><slot /></button></a>
    <link rel="stylesheet" href="https://cdn.simplecss.org/simple.min.css">
`;

class RouterLink extends HTMLElement {
    url: string;
    a: HTMLAnchorElement;
    constructor() {
        super();

        this.attachShadow({ mode: 'open' });
        if (this.shadowRoot === null) {
            throw new Error('Shadow root is null');
        }
        this.shadowRoot.append(template.content.cloneNode(true));

        const href = this.getAttribute('href');

        this.url = href ? href : '';

        const a = this.shadowRoot.querySelector('a');

        if (a === null) {
            throw new Error('A is null');
        }

        this.a = a;
        this.a.href = this.url;

        this.a.addEventListener('click', (event) => {
            event.preventDefault();
            navigateTo(this.url);
            RouterView.change();
        });
    }
}

window.customElements.define('router-link', RouterLink);
