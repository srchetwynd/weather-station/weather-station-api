import { router } from '../router/index.js';

const template = document.createElement('template');

export default class RouterView extends HTMLElement {
    constructor() {
        super();

        this.attachShadow({ mode: 'open' });
        this.shadowRoot!.append(template.content.cloneNode(true));

        document.addEventListener('DOMContentLoaded', () => this.render());
        window.addEventListener('popstate', RouterView.change);

        this.render();

        RouterView.views.push(this);
    }

    async render(): Promise<void> {
        const element = await router();

        if (this.shadowRoot !== null && this.shadowRoot !== undefined) {
            this.shadowRoot.innerHTML = ``;
            this.shadowRoot.append(document.createElement(element));
        }
    }

    static change(): void {
        for (const view of RouterView.views) {
            view.render();
        }
    }

    static views: RouterView[] = [];
}

window.customElements.define('router-view', RouterView);
