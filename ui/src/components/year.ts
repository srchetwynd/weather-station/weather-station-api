import('./charts.js');

const template = document.createElement('template');
template.innerHTML = `
    <div>
        <nav style="text-align: center">
            <button id="weekButton">
                <b>Weeks</b>
            </button>
            <button id="monthButton" disabled>
                <b>Months</b>
            </button>
        </nav>
        <nav style="text-align: center">
            <button id="backButton">
                <b>&lt;</b>
            </button>
            <span id="year">Year</span>
            <button id="forwardButton">
                <b>&gt;</b>
            </button>
        </nav>
        <weather-charts id="charts" weather-labels="[]" weather-data="[]" />
    </div>
    <link rel="stylesheet" href="https://cdn.simplecss.org/simple.min.css">
`;

export default class Year extends HTMLElement {
    weather: Record<string, any>[] = [];
    charts: HTMLElement;
    backButton: HTMLButtonElement;
    forwardButton: HTMLButtonElement;
    weekButton: HTMLButtonElement;
    monthButton: HTMLButtonElement;
    labels: string[] = [];
    date: Date;
    units = 'months';
    yearText: HTMLElement;
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.shadowRoot!.append(template.content.cloneNode(true));
        this.yearText = this.shadowRoot?.getElementById('year') as HTMLElement;
        this.charts = this.shadowRoot?.getElementById('charts') as HTMLElement;
        this.backButton = this.shadowRoot?.getElementById(
            'backButton'
        ) as HTMLButtonElement;
        this.forwardButton = this.shadowRoot?.getElementById(
            'forwardButton'
        ) as HTMLButtonElement;
        this.weekButton = this.shadowRoot?.getElementById(
            'weekButton'
        ) as HTMLButtonElement;
        this.monthButton = this.shadowRoot?.getElementById(
            'monthButton'
        ) as HTMLButtonElement;

        this.date = new Date();

        this.backButton.addEventListener('click', () => {
            this.date.setUTCFullYear(this.date.getUTCFullYear() - 1);
            this.render();
        });
        this.forwardButton.addEventListener('click', () => {
            this.date.setUTCFullYear(this.date.getUTCFullYear() + 1);
            this.render();
        });
        this.weekButton.addEventListener('click', () => {
            this.units = 'weeks';
            this.weekButton.setAttribute('disabled', 'true');
            this.monthButton.removeAttribute('disabled');
            this.render();
        });
        this.monthButton.addEventListener('click', () => {
            this.units = 'months';
            this.monthButton.setAttribute('disabled', 'true');
            this.weekButton.removeAttribute('disabled');
            this.render();
        });

        this.render();
    }

    async fetchData(): Promise<void> {
        const response = await fetch(
            `https://weather.srchetwynd.co.uk/api/years?unit=${
                this.units
            }&date=${this.date.toISOString().split('T')[0]}`
        );
        this.weather = await response.json();
        this.labels = [];

        const key = this.weather[0]?.month ? 'month' : 'week';
        for (let index = 0; index < this.weather.length; index++) {
            this.weather[index].label = this.weather[index][key];
            this.labels.push(this.weather[index][key]);
        }
    }

    async render(): Promise<void> {
        if (this.date.getTime() >= Date.now()) {
            this.forwardButton.setAttribute('disabled', 'true');
        } else {
            this.forwardButton.removeAttribute('disabled');
        }

        await this.fetchData();
        this.yearText.innerHTML = String(this.date.getFullYear());

        this.charts.setAttribute('weather-labels', JSON.stringify(this.labels));
        this.charts.setAttribute('weather-data', JSON.stringify(this.weather));
    }
}

window.customElements.define('weather-year', Year);
