import('./charts.js');

const template = document.createElement('template');
template.innerHTML = `
    <nav style="text-align: center">
        <button id="backButton">
            <b>&lt;</b>
        </button>
        <span id="day" class="col text-center">Day</span>
        <button id="forwardButton">
            <b>&gt;</b>
        </button>
    </nav>
    <weather-charts id="charts" weather-labels="[]" weather-data="[]" />
    <link rel="stylesheet" href="https://cdn.simplecss.org/simple.min.css">
`;

export default class Day extends HTMLElement {
    weather: Record<string, any>[] = [];
    charts: HTMLElement;
    backButton: HTMLButtonElement;
    forwardButton: HTMLButtonElement;
    date = new Date().toISOString().split('T')[0];
    dayText: HTMLElement;
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.shadowRoot!.append(template.content.cloneNode(true));
        this.dayText = this.shadowRoot?.getElementById('day') as HTMLElement;
        this.charts = this.shadowRoot?.getElementById('charts') as HTMLElement;
        this.backButton = this.shadowRoot?.getElementById(
            'backButton'
        ) as HTMLButtonElement;
        this.forwardButton = this.shadowRoot?.getElementById(
            'forwardButton'
        ) as HTMLButtonElement;

        this.backButton.addEventListener('click', () => {
            const date = new Date(this.date);
            date.setDate(date.getDate() - 1);
            this.date = date.toISOString().split('T')[0];
            this.render();
        });
        this.forwardButton.addEventListener('click', () => {
            const date = new Date(this.date);
            date.setDate(date.getDate() + 1);
            this.date = date.toISOString().split('T')[0];
            this.render();
        });

        const labels: string[] = [];

        for (let index = 0; index < 24; index++) {
            labels.push(String(index));
        }

        this.charts.setAttribute('weather-labels', JSON.stringify(labels));
        this.render();
    }

    async fetchData(): Promise<void> {
        const response = await fetch(
            `https://weather.srchetwynd.co.uk/api/days?date=${this.date}`
        );
        this.weather = await response.json();
        for (const hour of this.weather) {
            hour.label = String(hour.hour);
        }
    }

    async render(): Promise<void> {
        if (this.date === new Date().toISOString().split('T')[0]) {
            this.forwardButton.setAttribute('disabled', 'true');
        } else {
            this.forwardButton.removeAttribute('disabled');
        }

        this.dayText.innerHTML = this.date;

        await this.fetchData();

        this.charts.setAttribute('weather-data', JSON.stringify(this.weather));
    }
}

window.customElements.define('weather-day', Day);
