import Chart from 'chart.js';

const template = document.createElement('template');
template.innerHTML = `
    <article>
        <h2>
            Rain
        </h2>
        <div>
            <canvas id="day-rain" />
        </div>
    </article>
    <article>
        <h2>
            Humidity
        </h2>
        <div>
            <canvas id="day-humidity" />
        </div>
    </article>
    <article>
        <h2>
            Temperature
        </h2>
        <div>
            <canvas id="day-temperature" />
        </div>
    </article>
    <article>
        <h2>
            Wind
        </h2>
        <div>
            <canvas id="day-wind" />
        </div>
    </article>
    <link rel="stylesheet" href="https://cdn.simplecss.org/simple.min.css">
`;

export default class Charts extends HTMLElement {
    private weather: Record<string, any>[] = [];
    private labels: string[];
    private rainCanvas: HTMLCanvasElement;
    private humidityCanvas: HTMLCanvasElement;
    private temperatureCanvas: HTMLCanvasElement;
    private windCanvas: HTMLCanvasElement;
    private timeout: number | undefined;
    private rainChart: Chart | undefined;
    private humidityChart: Chart | undefined;
    private temperatureChart: Chart | undefined;
    private windChart: Chart | undefined;

    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.shadowRoot!.append(template.content.cloneNode(true));

        this.rainCanvas = this.shadowRoot?.getElementById(
            'day-rain'
        ) as HTMLCanvasElement;
        this.humidityCanvas = this.shadowRoot?.getElementById(
            'day-humidity'
        ) as HTMLCanvasElement;
        this.temperatureCanvas = this.shadowRoot?.getElementById(
            'day-temperature'
        ) as HTMLCanvasElement;
        this.windCanvas = this.shadowRoot?.getElementById(
            'day-wind'
        ) as HTMLCanvasElement;

        const weatherAttribute = this.getAttribute('weather-data');

        this.weather =
            weatherAttribute === undefined
                ? []
                : JSON.parse(String(weatherAttribute));

        const labelsAttribute = this.getAttribute('weather-labels');

        this.labels =
            labelsAttribute === undefined
                ? []
                : JSON.parse(String(labelsAttribute));

        this.render();
    }

    getData(type: string): (number | undefined)[] {
        const weatherData: (number | undefined)[] = [];
        for (const label of this.labels) {
            const date = this.weather.find((data) => data.label === label);
            if (date !== undefined) {
                weatherData.push(date[type]);
            } else {
                weatherData.push(undefined);
            }
        }
        return weatherData;
    }

    render(): void {
        if (this.timeout === undefined) {
            this.timeout = window.setTimeout((): void => {
                this.rainChart?.destroy();
                this.rainChart = new Chart(this.rainCanvas, {
                    type: 'line',
                    options: {
                        maintainAspectRatio: false,
                    },
                    data: {
                        labels: this.labels,
                        datasets: [
                            {
                                label: 'mm of Rainfall',
                                data: this.weather
                                    ? this.getData('rain_mm')
                                    : [0],
                                backgroundColor: 'rgba(26, 166, 154, 0.5)',
                            },
                        ],
                    },
                });

                this.humidityChart?.destroy();
                this.humidityChart = new Chart(this.humidityCanvas, {
                    type: 'line',
                    options: {
                        maintainAspectRatio: false,
                    },
                    data: {
                        labels: this.labels,
                        datasets: [
                            {
                                label: 'Humidity',
                                data: this.weather
                                    ? this.getData('humidity')
                                    : [0],
                                backgroundColor: 'rgba(171, 71, 188, 0.5)',
                            },
                        ],
                    },
                });

                this.temperatureChart?.destroy();
                this.temperatureChart = new Chart(this.temperatureCanvas, {
                    type: 'line',
                    options: {
                        maintainAspectRatio: false,
                    },
                    data: {
                        labels: this.labels,
                        datasets: [
                            {
                                label: 'Min Temperature C',
                                data: this.weather
                                    ? this.getData('min_temp')
                                    : [0],
                                backgroundColor: 'rgba(33, 150, 243, 0.5)',
                            },
                            {
                                label: 'Temperature C',
                                data: this.weather
                                    ? this.getData('temperature_c')
                                    : [0],
                                backgroundColor: 'rgba(239, 108, 0, 0.5)',
                            },
                            {
                                label: 'Max Temperature C',
                                data: this.weather
                                    ? this.getData('max_temp')
                                    : [0],
                                backgroundColor: 'rgba(224, 67, 54, 0.5)',
                            },
                        ],
                    },
                });

                this.windChart?.destroy();
                this.windChart = new Chart(this.windCanvas, {
                    type: 'line',
                    options: {
                        maintainAspectRatio: false,
                    },
                    data: {
                        labels: this.labels,
                        datasets: [
                            {
                                label: 'kph Average Wind Speed',
                                data: this.weather
                                    ? this.getData('wind_speed_average_kph')
                                    : [0],
                                backgroundColor: 'rgba(102, 187, 106, 0.5)',
                            },
                            {
                                label: 'kph Max Wind Speed',
                                data: this.weather
                                    ? this.getData('wind_speed_max_kph')
                                    : [0],
                                backgroundColor: 'rgba(212, 225, 87, 0.5)',
                            },
                        ],
                    },
                });
                this.timeout = undefined;
            }, 100);
        }
    }

    attributeChangedCallback(
        name: string,
        _oldValue: string,
        newValue: string
    ): void {
        if (name === 'weather-data') {
            this.weather = JSON.parse(String(newValue));
            this.render();
        } else if (name === 'weather-labels') {
            this.labels = JSON.parse(String(newValue));
            this.render();
        }
    }

    static get observedAttributes(): string[] {
        return ['weather-data', 'weather-labels'];
    }
}

window.customElements.define('weather-charts', Charts);
