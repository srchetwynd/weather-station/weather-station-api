import('./charts.js');

const template = document.createElement('template');
template.innerHTML = `
    <div>
        <div style="text-align: center">
            <button type="button" id="backButton">
                <b>&lt;</b>
            </button>
            <span>Year: <span id="yearText"></span> Week: <span id="weekText"></span><span>
            <button type="button" id="forwardButton">
                <b>&gt;</b>
            </button>
        </div>
        <weather-charts id="charts" weather-labels="[]" weather-data="[]" />
    </div>
    <link rel="stylesheet" href="https://cdn.simplecss.org/simple.min.css">
`;

export default class Week extends HTMLElement {
    weather: Record<string, any>[] = [];
    charts: HTMLElement;
    backButton: HTMLButtonElement;
    forwardButton: HTMLButtonElement;
    date: Date;
    week = 0;
    weekText: HTMLElement;
    yearText: HTMLElement;
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.shadowRoot!.append(template.content.cloneNode(true));
        this.charts = this.shadowRoot?.getElementById('charts') as HTMLElement;
        this.backButton = this.shadowRoot?.getElementById(
            'backButton'
        ) as HTMLButtonElement;
        this.forwardButton = this.shadowRoot?.getElementById(
            'forwardButton'
        ) as HTMLButtonElement;
        this.weekText = this.shadowRoot?.getElementById(
            'weekText'
        ) as HTMLElement;
        this.yearText = this.shadowRoot?.getElementById(
            'yearText'
        ) as HTMLElement;

        this.date = new Date();

        this.backButton.addEventListener('click', () => {
            this.date.setDate(this.date.getDate() - 7);
            this.render();
        });
        this.forwardButton.addEventListener('click', () => {
            this.date.setDate(this.date.getDate() + 7);
            this.render();
        });

        this.render();
    }

    setWeek(): void {
        const onejan = new Date(new Date(this.date).getFullYear(), 0, 1);

        this.week = Math.ceil(
            ((this.date.getTime() - onejan.getTime()) / 86_400_000 +
                onejan.getDay() +
                1) /
                7 -
                1
        );
    }

    async fetchData(): Promise<void> {
        const response = await fetch(
            `https://weather.srchetwynd.co.uk/api/weeks?date=${
                this.date.toISOString().split('T')[0]
            }`
        );
        this.weather = await response.json();
        for (const data of this.weather) {
            data.label = data.date.split(' ')[0];
        }
    }

    async render(): Promise<void> {
        this.setWeek();
        if (this.date.getTime() >= Date.now()) {
            this.forwardButton.setAttribute('disabled', 'true');
        } else {
            this.forwardButton.removeAttribute('disabled');
        }

        await this.fetchData();
        this.weekText.innerHTML = String(this.week);
        this.yearText.innerHTML = String(this.date.getFullYear());

        this.charts.setAttribute(
            'weather-labels',
            JSON.stringify(this.weather.map((data) => data.label))
        );
        this.charts.setAttribute('weather-data', JSON.stringify(this.weather));
    }
}

window.customElements.define('weather-week', Week);
