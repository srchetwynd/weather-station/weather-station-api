const template = document.createElement('template');
template.innerHTML = `
    <button>
        <slot>
    </button>
    <link rel="stylesheet" href="https://cdn.simplecss.org/simple.min.css">
`;

export default class Button extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.shadowRoot!.append(template.content.cloneNode(true));
    }
}

window.customElements.define('weather-button', Button);
