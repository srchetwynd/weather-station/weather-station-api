import * as Sentry from '@sentry/browser';
import { Integrations } from '@sentry/tracing';
import { VERSION } from './version.js';

Sentry.init({
    dsn: 'https://03195875702e4064a96ef464e3bbd16f@o280795.ingest.sentry.io/5549643',

    // Alternatively, use `process.env.npm_package_version` for a dynamic release version
    // if your build tool supports it.
    release: 'weather-front@' + VERSION,
    integrations: [new Integrations.BrowserTracing()],

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1,
});

import('./components/router-link.js');
import('./components/router-view.js');
