cat > ./docker-compose.yml << EOF
version: "3"

services:
  weather-station-front:
    image: registry.gitlab.com/srchetwynd/weather-station/weather-station-front:$1
    restart: unless-stopped
    networks:
      - nginx-proxy-manager

networks:
  nginx-proxy-manager:
    external: true
    name: nginx-proxy-manager
EOF
