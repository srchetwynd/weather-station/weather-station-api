package main

import (
	"net/http"
	"srchetwynd.co.uk/weather/api/src/config"
	"srchetwynd.co.uk/weather/api/src/handlers"
	"srchetwynd.co.uk/weather/api/src/postgres"
	"srchetwynd.co.uk/weather/api/src/repository"
	"srchetwynd.co.uk/weather/api/src/router"
)

func main() {
	conf := config.CreateConfig()

	db := postgres.CreateConnection(conf)

	repository := repository.CreateRepository(db)

	h := handlers.CreateHandlers(repository)

	r := router.CreateRouter(&h)

	http.ListenAndServe(":3000", r)
}
