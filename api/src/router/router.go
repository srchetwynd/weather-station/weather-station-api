package router

import (
  "net/http"
  "github.com/go-chi/chi/v5"
  "github.com/go-chi/chi/v5/middleware"
  "github.com/go-chi/cors"
)

type Handlers interface {
  GetDays(http.ResponseWriter, *http.Request)
  GetWeeks(http.ResponseWriter, *http.Request)
  GetMonths(http.ResponseWriter, *http.Request)
  GetYears(http.ResponseWriter, *http.Request)
  Health(http.ResponseWriter, *http.Request)
}

func CreateRouter(h Handlers) chi.Router {
  r := chi.NewRouter()

  cors := cors.New(cors.Options{
    AllowedOrigins:   []string{"*"},
    // AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
    AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
    AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
    ExposedHeaders:   []string{"Link"},
    AllowCredentials: true,
    MaxAge:           300, // Maximum value not ignored by any of major browsers
  })
  r.Use(cors.Handler)    
  r.Use(middleware.Logger)

  r.Get("/days", h.GetDays)
  r.Get("/weeks", h.GetWeeks)
  r.Get("/months", h.GetMonths)
  r.Get("/years", h.GetYears)
  r.Get("/health", h.Health)

  return r
}
