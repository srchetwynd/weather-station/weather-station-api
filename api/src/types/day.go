package types

type Day struct {
  TemperatureC float32 `json:"temperature_c"`
  MaxTemp float32 `json:"max_temp"`
  MinTemp float32 `json:"min_temp"`
  RainMM float32 `json:"rain_mm"`
  WindSpeedAverageKPH float32 `json:"wind_speed_average_kph"`
  WindSpeedMaxKPH float32 `json:"wind_speed_max_kph"`
  Humidity float32 `json:"humidity"`
  Hour int `json:"hour"`
  Date string `json:"date"`
}
