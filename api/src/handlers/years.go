package handlers

import (
  "net/http"
  "time"
  "encoding/json"
)

func (h *Handlers) GetYears(w http.ResponseWriter, r *http.Request) {
  dateParam := r.URL.Query().Get("date")
  
  if dateParam == "" {
    t := time.Now()

    dateParam = t.Format("2006-01-02")
  }

  _, err := time.Parse("2006-01-02", dateParam)
  
  unitParam := r.URL.Query().Get("unit")

  if unitParam == "" {
    unitParam = "weeks"
  }

  if unitParam != "weeks" && unitParam != "months" {
    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(400)
    json.NewEncoder(w).Encode(HttpError{ Message: "Invalid unit: Unit must be 'months' or 'weeks'" })
    return
  }

  if err != nil {
    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(400)
    json.NewEncoder(w).Encode(HttpError{ Message: "Invalid Date: Date must be in YYYY-MM-DD format." })
    return
  }

  byWeeks, byMonths := h.dataConnection.GetYear(dateParam, unitParam)

  w.Header().Set("Content-Type", "application/json")

  if byWeeks != nil {
    json.NewEncoder(w).Encode(byWeeks)
    return
  }

  json.NewEncoder(w).Encode(byMonths)
}
