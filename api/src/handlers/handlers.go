package handlers

import (
  "srchetwynd.co.uk/weather/api/src/types"
)

type dataConnection interface {
  GetDay(string) *[]types.Day
  GetWeek(string) *[]types.Month
  GetMonth(string) *[]types.Month
  GetYear(string, string) (*[]types.YearByWeek, *[]types.YearByMonth)
}

type Handlers struct {
  dataConnection dataConnection
}

type HttpError struct {
  Message string `json:"message"`
}

func CreateHandlers(repository dataConnection) Handlers {
  return Handlers{ dataConnection: repository }
}
