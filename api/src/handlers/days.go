package handlers

import (
  "net/http"
  "time"
  "encoding/json"
)

func (h *Handlers) GetDays(w http.ResponseWriter, r *http.Request) {
  dateParam := r.URL.Query().Get("date")

  if dateParam == "" {
    t := time.Now()

    dateParam = t.Format("2006-01-02")
  }

  _, err := time.Parse("2006-01-02", dateParam)

  if err != nil {
    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(400)
    json.NewEncoder(w).Encode(HttpError{ Message: "Invalid Date: Date must be in YYYY-MM-DD format." })
    return
  }

  result := h.dataConnection.GetDay(dateParam)

  w.Header().Set("Content-Type", "application/json")
  json.NewEncoder(w).Encode(result)
}
