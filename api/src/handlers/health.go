package handlers

import (
  "net/http"
)

func (h *Handlers) Health(w http.ResponseWriter, r *http.Request) {
  w.WriteHeader(200)
}
