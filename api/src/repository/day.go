package repository

import (
  "log"
  "srchetwynd.co.uk/weather/api/src/types"
)

func (r *Repository) GetDay(date string) *[]types.Day {
  rows, err := r.db.Query(`
    SELECT
      CAST(AVG(temperature_c) AS float(8)) AS temperature_c,
      CAST(MAX(temperature_c) AS float(8)) AS max_temp,
      CAST(MIN(temperature_c) AS float(8)) AS min_temp,
      CAST(MAX(rain_mm) - MIN(rain_mm) AS float(8)) AS rain_mm,
      CAST(AVG(wind_speed_average_kph) AS float(8)) AS wind_speed_average_kph,
      CAST(MAX(wind_speed_max_kph) AS float(8)) AS wind_speed_max_kph,
      CAST(AVG(humidity) AS float(8)) AS humidity,
      cast(date_part('hour', time) as int) AS hour,
      CONCAT(
        date_part('year', time),
        '-',
        date_part('month', time),
        '-',
        date_part('day', time) 
    ) AS date
      FROM
        weather
      WHERE
        time::date = $1::date
      GROUP BY
        date_part('year', time),
        date_part('month', time),
        date_part('day', time),
        date_part('hour', time)
      ORDER BY
        hour
    `,
    date,
  )

  if err != nil {
    log.Fatal(err)
  }

  defer rows.Close()

  results := make([]types.Day, 0)

  for rows.Next() {
    var temperatureC float32 
    var maxTemp float32 
    var minTemp float32 
    var rainMM float32 
    var windSpeedAverageKPH float32 
    var windSpeedMaxKPH float32
    var humidity float32
    var hour int
    var date string

    err := rows.Scan(
      &temperatureC,
      &maxTemp,
      &minTemp,
      &rainMM,
      &windSpeedAverageKPH,
      &windSpeedMaxKPH,
      &humidity,
      &hour,
      &date,
    )

    if err != nil {
      log.Fatal(err)
    }

    day := types.Day{
      TemperatureC: temperatureC,
      MaxTemp: maxTemp,
      MinTemp: minTemp,
      RainMM: rainMM,
      WindSpeedAverageKPH: windSpeedAverageKPH,
      WindSpeedMaxKPH: windSpeedMaxKPH,
      Humidity: humidity,
      Hour: hour,
      Date: date,
    }

    results = append(results, day)
  }

  if err := rows.Err(); err != nil {
    log.Fatal(err)
  }

  return &results
}
