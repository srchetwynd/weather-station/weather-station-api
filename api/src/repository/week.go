package repository

import (
  "log"
  "srchetwynd.co.uk/weather/api/src/types"
  "time"
)

func (r *Repository) GetWeek(date string) *[]types.Month {
  t, timeParseErr := time.Parse("2006-01-02", date)

  if timeParseErr != nil {
    log.Fatal(timeParseErr)
  }

  _, week := t.ISOWeek()

  rows, err := r.db.Query(`
    SELECT
      CAST(AVG(temperature_c) AS float(8)) AS temperature_c,
      CAST(MAX(temperature_c) AS float(8)) AS max_temp,
      CAST(MIN(temperature_c) AS float(8)) AS min_temp,
      CAST(MAX(rain_mm) - MIN(rain_mm) AS float(8)) AS rain_mm,
      CAST(AVG(wind_speed_average_kph) AS float(8)) AS wind_speed_average_kph,
      CAST(MAX(wind_speed_max_kph) AS float(8)) AS wind_speed_max_kph,
      CAST(AVG(humidity) AS float(8)) AS humidity,
      date_trunc('day', time)::TEXT AS date
    FROM
      weather
    WHERE
      date_part('year', time) = $1 AND
      date_part('week', time) = $2
    GROUP BY
      date_trunc('day', time)
    ORDER BY
      date
    `,
    t.Year(),
    week,
  )

  if err != nil {
    log.Fatal(err)
  }

  defer rows.Close()

  results := make([]types.Month, 0)

  for rows.Next() {
    var temperatureC float32 
    var maxTemp float32 
    var minTemp float32 
    var rainMM float32 
    var windSpeedAverageKPH float32 
    var windSpeedMaxKPH float32
    var humidity float32
    var date string

    err := rows.Scan(
      &temperatureC,
      &maxTemp,
      &minTemp,
      &rainMM,
      &windSpeedAverageKPH,
      &windSpeedMaxKPH,
      &humidity,
      &date,
    )

    if err != nil {
      log.Fatal(err)
    }

    day := types.Month{
      TemperatureC: temperatureC,
      MaxTemp: maxTemp,
      MinTemp: minTemp,
      RainMM: rainMM,
      WindSpeedAverageKPH: windSpeedAverageKPH,
      WindSpeedMaxKPH: windSpeedMaxKPH,
      Humidity: humidity,
      Date: date,
    }

    results = append(results, day)
  }

  if err := rows.Err(); err != nil {
    log.Fatal(err)
  }

  return &results
}
