package repository

import (
  "log"
  "srchetwynd.co.uk/weather/api/src/types"
  "time"
)

func (r *Repository) GetYear(date string, unit string) (*[]types.YearByWeek, *[]types.YearByMonth) {
  t, err := time.Parse("2006-01-02", date)

  if err != nil {
    log.Fatal(err)
  }

  year := t.Year()

  if unit == "months" {
    return nil, r.getYearByMonths(year)
  }

  return r.getYearByWeeks(year), nil
}

func (r *Repository) getYearByMonths(year int) *[]types.YearByMonth {
  rows := r.mustQuery(`
    SELECT
      CAST(AVG(temperature_c) AS float(8)) AS temperature_c,
      CAST(MAX(temperature_c) AS float(8)) AS max_temp,
      CAST(MIN(temperature_c) AS float(8)) AS min_temp,
      CAST(MAX(rain_mm) - MIN(rain_mm) AS float(8)) AS rain_mm,
      CAST(AVG(wind_speed_average_kph) AS float(8)) AS wind_speed_average_kph,
      CAST(MAX(wind_speed_max_kph) AS float(8)) AS wind_speed_max_kph,
      CAST(AVG(humidity) AS float(8)) AS humidity,
      CAST(date_part('month', time) AS int) AS month
    FROM
      weather
    WHERE
      DATE_PART('year', time) = $1
    GROUP BY
      date_part('year', time),
      date_part('month', time)
    ORDER BY
      month
    `,
    year,
  )
  
  defer rows.Close()

  results := make([]types.YearByMonth, 0)

  for rows.Next() {
    var temperatureC float32 
    var maxTemp float32 
    var minTemp float32 
    var rainMM float32 
    var windSpeedAverageKPH float32 
    var windSpeedMaxKPH float32
    var humidity float32
    var month string

    err := rows.Scan(
      &temperatureC,
      &maxTemp,
      &minTemp,
      &rainMM,
      &windSpeedAverageKPH,
      &windSpeedMaxKPH,
      &humidity,
      &month,
    )

    if err != nil {
      log.Fatal(err)
    }

    day := types.YearByMonth{
      TemperatureC: temperatureC,
      MaxTemp: maxTemp,
      MinTemp: minTemp,
      RainMM: rainMM,
      WindSpeedAverageKPH: windSpeedAverageKPH,
      WindSpeedMaxKPH: windSpeedMaxKPH,
      Humidity: humidity,
      Month: month,
    }

    results = append(results, day)
  }

  if err := rows.Err(); err != nil {
    log.Fatal(err)
  }

  return &results
}

func (r *Repository) getYearByWeeks(year int) *[]types.YearByWeek {
  rows := r.mustQuery(`
    SELECT
      CAST(AVG(temperature_c) AS float(8)) AS temperature_c,
      CAST(MAX(temperature_c) AS float(8)) AS max_temp,
      CAST(MIN(temperature_c) AS float(8)) AS min_temp,
      CAST(MAX(rain_mm) - MIN(rain_mm) AS float(8)) AS rain_mm,
      CAST(AVG(wind_speed_average_kph) AS float(8)) AS wind_speed_average_kph,
      CAST(MAX(wind_speed_max_kph) AS float(8)) AS wind_speed_max_kph,
      CAST(AVG(humidity) AS float(8)) AS humidity,
      CAST(date_part('week', time) AS int) AS week
    FROM
      weather
    WHERE
      DATE_PART('year', time) = $1
    GROUP BY
      date_part('year', time),
      date_part('week', time)
    ORDER BY
      week
    `,
    year,
  )
  
  defer rows.Close()

  results := make([]types.YearByWeek, 0)

  for rows.Next() {
    var temperatureC float32 
    var maxTemp float32 
    var minTemp float32 
    var rainMM float32 
    var windSpeedAverageKPH float32 
    var windSpeedMaxKPH float32
    var humidity float32
    var week string

    err := rows.Scan(
      &temperatureC,
      &maxTemp,
      &minTemp,
      &rainMM,
      &windSpeedAverageKPH,
      &windSpeedMaxKPH,
      &humidity,
      &week,
    )

    if err != nil {
      log.Fatal(err)
    }

    day := types.YearByWeek{
      TemperatureC: temperatureC,
      MaxTemp: maxTemp,
      MinTemp: minTemp,
      RainMM: rainMM,
      WindSpeedAverageKPH: windSpeedAverageKPH,
      WindSpeedMaxKPH: windSpeedMaxKPH,
      Humidity: humidity,
      Week: week,
    }

    results = append(results, day)
  }

  if err := rows.Err(); err != nil {
    log.Fatal(err)
  }

  return &results
}
