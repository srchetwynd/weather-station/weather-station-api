package config

import (
  "fmt"
  "os"
)

type Config struct {
  dbUrl string
}

func (c *Config) GetDbUrl() string {
  if c.dbUrl == "" {
    c.dbUrl = mustExist("DB_URL")
  }

  return c.dbUrl
}

func mustExist(key string) string {
  v := os.Getenv(key)

  if v == "" {
    panic(fmt.Sprintf("%s is required but not set.", key))
  }

  return v
}

func CreateConfig() *Config {
  return &Config{}
}
