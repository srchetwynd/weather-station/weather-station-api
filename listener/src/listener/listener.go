package listener

import (
  mqtt "github.com/eclipse/paho.mqtt.golang"
	"srchetwynd.co.uk/weather/listener/src/types"
	"strconv"
)

type config interface {
  GetMQTTBrokerURL() string
}

var baseTopic = "rtl_433/80/";

func RunListener(c chan<- *types.WeatherSensor, e chan<- error, i chan<- string, conf config) {
  broker := conf.GetMQTTBrokerURL()

  opts := mqtt.NewClientOptions()
  opts.AddBroker(broker)
  opts.SetClientID("weather_web_site")
  opts.OnConnect = func(client mqtt.Client) {
    createSubscriptions(client, c)
    i <- "MQTT Connected"
  }
  opts.OnConnectionLost = func(client mqtt.Client, err error) {
    e <- err
  }

  client := mqtt.NewClient(opts)
  
  if token := client.Connect(); token.Wait() && token.Error() != nil {
    panic(token.Error())
  }
}
  
func createSubscriptions(client mqtt.Client, c chan<- *types.WeatherSensor) {
  timeChannel := make(chan string)
  batteryChannel := make(chan string)
  idChannel := make(chan int)
  humidityChannel := make(chan int)
  temperatureChannel := make(chan float64)
  rainChannel := make(chan float64)
  windDirChannel := make(chan float64)
  windAvgChannel := make(chan float64)
  windMaxChannel := make(chan float64)

  topicStringSubscriber(client, "time", timeChannel)
  topicStringSubscriber(client, "battery_ok", batteryChannel)
  topicIntSubscriber(client, "id", idChannel)
  topicIntSubscriber(client, "humidity", humidityChannel)
  topicFloatSubscriber(client, "temperature_C", temperatureChannel)
  topicFloatSubscriber(client, "rain_mm", rainChannel)
  topicFloatSubscriber(client, "wind_dir_deg", windDirChannel)
  topicFloatSubscriber(client, "wind_avg_km_h", windAvgChannel)
  topicFloatSubscriber(client, "wind_max_km_h", windMaxChannel)

  go func() {
    for {
      time := <-timeChannel
      battery := <- batteryChannel
      id := <- idChannel
      temperature := <- temperatureChannel
      rain := <- rainChannel
      windDir := <- windDirChannel
      windAvg := <- windAvgChannel
      windMax := <- windMaxChannel
      humidity := <- humidityChannel

      data := types.WeatherSensor{
        Time: time,
        Model: "",
        Brand: "",
        Sid: 0,
        Id: id,
        Rc: 0,
        Channel: 0,
        Battery: battery,
        Temperature_C: temperature,
        Rain_MM: rain,
        Wind_Dir_Deg: windDir,
        Wind_Avg_KM_H: windAvg,
        Wind_Max_KM_H: windMax,
        Temperature_F: (temperature * 1.8) + 32,
        Humidity: humidity,
      }

      c <- &data
    }
  }()
}

func topicStringSubscriber(client mqtt.Client, topic string, channel chan<- string) {
  token := client.Subscribe(baseTopic + topic, 1, func(client mqtt.Client, message mqtt.Message) {
    go func() {
      value := string(message.Payload()[:])   

      channel <- value
    }()
  })
  
  token.Wait()

  if token.Error() != nil {
    panic(token.Error())
  }
}

func topicIntSubscriber(client mqtt.Client, topic string, channel chan<- int) {
  token := client.Subscribe(baseTopic + topic, 1, func(client mqtt.Client, message mqtt.Message) {
    go func() {
      value, err := strconv.Atoi(string(message.Payload()[:]))

      if err != nil {
        panic(err)
      }
      
      channel <- value
    }()
  })
  
  token.Wait()

  if token.Error() != nil {
    panic(token.Error())
  }
}

func topicFloatSubscriber(client mqtt.Client, topic string, channel chan<- float64) {
  token := client.Subscribe(baseTopic + topic, 1, func(client mqtt.Client, message mqtt.Message) {
    go func() {
      value, err := strconv.ParseFloat(string(message.Payload()[:]), 64)
      
      if err != nil {
        panic(err)
      }

      channel <- value
    }()
  })

  token.Wait()

  if token.Error() != nil {
    panic(token.Error())
  }
}
