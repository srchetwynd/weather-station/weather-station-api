package config

import (
	"fmt"
	"os"
)

type Config struct {
	dbUrl string
	commitSha string
	mqttBrokerUrl string
}

func (c *Config) GetDbUrl() string {
	if c.dbUrl == "" {
		c.dbUrl = mustExist("DB_URL")
	}

	return c.dbUrl
}

func (c *Config) GetCommitSha() string {
	if c.commitSha == "" {
		c.commitSha = mayExist("COMMIT_SHA", "NO_COMMIT_SHA")
	}

	return c.commitSha
}

func (c *Config) GetMQTTBrokerURL() string {
	if c.mqttBrokerUrl == "" {
		c.mqttBrokerUrl = mustExist("MQTT_BROKER_URL")
	}

	return c.mqttBrokerUrl
}

func mustExist(key string) string {
	v := os.Getenv(key)

	if v == "" {
		panic(fmt.Sprintf("%s is required but not set.", key))
	}

	return v
}

func mayExist(key, defaultValue string) string {
	v := os.Getenv(key)

	if v == "" {
		return defaultValue
	}

	return v
}

func CreateConfig() *Config {
	return &Config{}
}
