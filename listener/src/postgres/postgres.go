package postgres

import (
	"database/sql"
	_ "github.com/lib/pq"
	"log"
)

type config interface {
	GetDbUrl() string
}

func CreateConnection(conf config) *sql.DB {
	db, err := sql.Open("postgres", conf.GetDbUrl())

	if err != nil {
		log.Fatal(err)
	}

	return db
}
