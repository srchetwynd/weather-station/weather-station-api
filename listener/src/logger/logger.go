package logger

import (
	"fmt"
)

type Logger struct {
	e chan error
	i chan string
	d chan string
}

func (l *Logger) start() {
	for {
		select {
		case m := <-l.e:
			fmt.Println("\033[0;31m [ERROR]:\033[0m " + fmt.Sprintf("%v", m))
		case m := <-l.i:
			fmt.Println("\033[0;33m [INFO]:\033[0m " + m)
		case m := <-l.d:
			fmt.Println("\033[0;36m [DEBUG]:\033[0m " + m)
		}
	}
}

func CreateLogger(e chan error, i, d chan string) *Logger {
	l := Logger{e: e, i: i, d: d}
	l.start()

	return &l
}
