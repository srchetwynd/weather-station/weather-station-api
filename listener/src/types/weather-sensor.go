package types

type WeatherSensor struct {
	Time          string
	Model         string
	Brand         string
	Sid           int
	Id            int
	Rc            int
	Channel       int
	Battery       string
	Temperature_C float64
	Rain_MM       float64
	Wind_Dir_Deg  float64
	Wind_Avg_KM_H float64
	Wind_Max_KM_H float64
	Temperature_F float64
	Humidity      int
}
