package repository

import (
	"database/sql"
	"log"
)

type connection interface {
	Query(sql string, parameters ...any) (*sql.Rows, error)
}

type Repository struct {
	db connection
}

func CreateRepository(db connection) *Repository {
	return &Repository{db: db}
}

func (r *Repository) mustQuery(sql string, parameters ...any) *sql.Rows {
	rows, err := r.db.Query(sql, parameters...)

	if err != nil {
		log.Fatal(err)
	}

	defer rows.Close()

	return rows
}
