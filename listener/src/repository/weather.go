package repository

import (
	"srchetwynd.co.uk/weather/listener/src/types"
)

func (r *Repository) InsertWeather(weather *types.WeatherSensor) {
	r.mustQuery(`
      INSERT INTO weather 
        (
          temperature_c,
          rain_mm,
          wind_speed_average_kph,
          wind_speed_max_kph,
          wind_direction,
          humidity,
          time
        )
      VALUES
        ($1, $2, $3, $4, $5, $6, NOW())
    `,
		weather.Temperature_C,
		weather.Rain_MM,
		weather.Wind_Avg_KM_H,
		weather.Wind_Max_KM_H,
		weather.Wind_Dir_Deg,
		weather.Humidity,
	)
}
