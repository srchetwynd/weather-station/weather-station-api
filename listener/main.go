package main

import (
	"fmt"
	"srchetwynd.co.uk/weather/listener/src/config"
	"srchetwynd.co.uk/weather/listener/src/listener"
	"srchetwynd.co.uk/weather/listener/src/logger"
	"srchetwynd.co.uk/weather/listener/src/postgres"
	"srchetwynd.co.uk/weather/listener/src/repository"
	"srchetwynd.co.uk/weather/listener/src/types"
)

func main() {
	conf := config.CreateConfig()

	fmt.Println("Listener Starting, Commit Sha: ", conf.GetCommitSha())

	db := postgres.CreateConnection(conf)

	repository := repository.CreateRepository(db)

	c := make(chan *types.WeatherSensor)
	e := make(chan error)
	i := make(chan string)
	d := make(chan string)

	go logger.CreateLogger(e, i, d)

	go listener.RunListener(c, e, i, conf)

	for {
		sensorReading := <-c
		repository.InsertWeather(sensorReading)
	}
}
